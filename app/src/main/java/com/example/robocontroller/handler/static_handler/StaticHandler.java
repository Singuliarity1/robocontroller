package com.example.robocontroller.handler.static_handler;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceView;

public class StaticHandler {
    public static int x = 90;
    public static int y = 0;
    public static int height = 0;
    public static int width = 0;
    public static int alpha = 0;
    public static boolean first_set = true;
    public static Vibrator vib;

    public static void setXY(int x, int y) {
        StaticHandler.x = x;
        StaticHandler.y = y;
        first_set = false;
    }

    public static void setDisplayParams(Canvas canvas) {
        StaticHandler.height = canvas.getHeight();
        StaticHandler.width = canvas.getWidth();
        if(first_set) StaticHandler.x = canvas.getWidth() / 2;
    }

    public static int setAlpha(int positions) {
        int short_width = !first_set ? StaticHandler.width - x : StaticHandler.width / 2;
        StaticHandler.alpha = Math.round(180 * short_width / StaticHandler.width);
        return StaticHandler.alpha;
    }
}
