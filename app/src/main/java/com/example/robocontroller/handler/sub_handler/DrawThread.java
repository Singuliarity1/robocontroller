package com.example.robocontroller.handler.sub_handler;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;

import com.example.robocontroller.handler.BluetoothHandler;
import com.example.robocontroller.handler.static_handler.StaticHandler;

public class DrawThread extends Thread {

    private SurfaceHolder sh;
    private boolean running = false;
    private Paint p;

    public DrawThread(SurfaceHolder sh) {
        this.p = new Paint();
        this.p.setColor(Color.WHITE);
        this.p.setStrokeWidth(5);
        this.p.setStyle(Paint.Style.FILL);
        this.sh = sh;



    }


    private void drawController(Canvas canvas) {
        canvas.drawCircle(StaticHandler.x, StaticHandler.y, 100, this.p);
    }

    private int getXProjection() {
        StaticHandler.x=StaticHandler.x==0?StaticHandler.width/2:StaticHandler.x;
        int x =  StaticHandler.x;
        x = Math.max(x, 0);
        x = Math.min(x, StaticHandler.width);
        StaticHandler.setAlpha(x);
        return x;
    }

    private void drawUSPosition(Canvas canvas) {
        canvas.drawLine(Math.round(StaticHandler.width / 2), StaticHandler.height, this.getXProjection(), 0, this.p);

    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    @Override
    public void run() {
        Canvas canvas;

        while (running) {
            canvas = null;
            try {
                canvas = sh.lockCanvas(null);

                if (canvas == null) continue;
                canvas.drawColor(Color.BLUE);
                StaticHandler.setDisplayParams(canvas);
                this.drawUSPosition(canvas);


            } finally {
                if (canvas != null) sh.unlockCanvasAndPost(canvas);
            }
        }
    }
}
