package com.example.robocontroller.handler;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

public class BluetoothHandler {

    BluetoothAdapter bluetooth;
    BluetoothDataTranslator bt;
    Context ctx;
    final String ADDR_BLUETOOTH = "00:20:12:08:39:59";

    public BluetoothHandler(Context ctx) throws Exception {
        this.ctx = ctx;
        bluetooth = BluetoothAdapter.getDefaultAdapter();
        if (bluetooth == null) {
            throw new Exception("Bluetooth Error");
        }

        if (!bluetooth.isEnabled()) {
            Toast.makeText(ctx, "Enable Bluetooth", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(ctx, "Bluetooth is enabled", Toast.LENGTH_LONG).show();
        }

    }

    public void connectToRobot() throws IOException {
        Set<BluetoothDevice> bd = bluetooth.getBondedDevices();

        if (bd.size() > 0) {
            for (BluetoothDevice device : bd) {
                if (device.getAddress().equals(ADDR_BLUETOOTH)) {
                    Log.d("DEVCIS", device.getAddress());
                    bt = new BluetoothDataTranslator(device);
                    bt.start();
                }
                Log.d("devices", device.getName() + "-" + device.getAddress());
            }
        } else {
            Toast.makeText(ctx, "Devices not found", Toast.LENGTH_LONG).show();
        }
    }

    public BluetoothDataTranslator getBt() {
        return bt;
    }
}
