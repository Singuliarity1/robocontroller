package com.example.robocontroller;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.robocontroller.handler.BluetoothDataTranslator;
import com.example.robocontroller.handler.BluetoothHandler;
import com.example.robocontroller.handler.DrawHandler;
import com.example.robocontroller.handler.static_handler.StaticHandler;

import java.util.ArrayList;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener {

    TextView tv;
    BluetoothHandler bh;
    DrawHandler draw;
    Vibrator vib;
    SurfaceView sv;
    Button reconnect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sv = (SurfaceView) findViewById(R.id.surfaceView);
        draw = new DrawHandler(this);
        sv.getHolder().addCallback(draw);
        sv.setOnTouchListener(this);
        vib = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        try {
            bh = new BluetoothHandler(this);
            View.OnClickListener btnRecon = v -> {
                try {
                    this.bh.connectToRobot();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            };
            reconnect = (Button) findViewById(R.id.reconnect);
            reconnect.setOnClickListener(btnRecon);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Objects.requireNonNull(getSupportActionBar()).hide();
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        vib.vibrate(5);
        StaticHandler.setXY((int) event.getX(), (int) event.getY());
        if (bh.getBt() != null) {
            bh.getBt().setAlphaInfo(StaticHandler.alpha);
        }
        return true;
    }
}